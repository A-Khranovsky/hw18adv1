<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('author_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('author_id')->references('id')
                ->on('users')->onDelete('SET NULL');
        });

        Schema::table('project_user', function (Blueprint $table) {
            $table->foreignId('project_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('id')
                ->on('projects')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_users', function(Blueprint $table){
            $table->dropConstrainedForeignId('project_id');
        });

        Schema::dropIfExists('projects');
    }
}
