<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
            $table->foreignId('author_id')->unsigned()->nullable();
            $table->foreign('author_id')->references('id')
                ->on('users')->onDelete('SET NULL');
        });

        Schema::table('label_project', function (Blueprint $table) {
            $table->foreignId('label_id')->unsigned()->nullable();
            $table->foreign('label_id')->references('id')
                ->on('labels')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('label_project', function (Blueprint $table) {
            $table->dropConstrainedForeignId('label_id');
        });

        Schema::dropIfExists('labels');
    }
}
