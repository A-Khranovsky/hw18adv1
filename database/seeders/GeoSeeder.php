<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Continent;
use App\Models\Country;

class GeoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $path = database_path('data' . DIRECTORY_SEPARATOR . 'continents.json');
        $content = file_get_contents($path);

        $continentItems = json_decode($content, true);

        $data = [];
        foreach (array_unique($continentItems) as $item) {
            $data[] = ['code' => $item,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        };

        Continent::insert($data);

        $continents = Continent::all()->pluck('id', 'code');

        $path = database_path('data'. DIRECTORY_SEPARATOR . 'countries.json');
        $content = file_get_contents($path);
        $countryItems = json_decode($content, true);

        $data = [];

        foreach ($continentItems as $countryCode => $continentCode) {
            $data[] = [
                'code' => $countryCode,
                'name' => $countryItems[$countryCode],
                'continent_id' => $continents[$continentCode],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        };

        Country::insert($data);
    }
}
