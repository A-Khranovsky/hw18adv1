```
REST API CRUD in Laravel with queue, migrations, seeder

```
## Up the services
docker-compose up -d

## Go to the container
docker exec -it hw18adv1_php-apache_1 bash

## Run inside the container
php artisan migrate:fresh --seed

## Use queries similar those on the client side:

### Users

* Add user
```
PUT http://localhost:80/api/users/add
Content-Type: application/json
Accept: application/json

{
  "name": "Alex",
  "email": "alex_some@gmail.com",
  "password": "password",
  "country": "France"
}

###
```

* Verify
```
GET http://localhost/api/users/verify/alex_some@gmail.com/uWNzAFcMHt
Accept: application/json

###
```

* List users (filter by name OR/AND email OR/AND verified OR/AND country)
```
GET http://localhost:80/api/users?name=Alexander
Accept: application/json

###

GET http://localhost:80/api/users?email=alexander@gmail.com
Accept: application/json

###

GET http://localhost:80/api/users?country_code=UA
Accept: application/json

###

GET http://localhost:80/api/users?name=Alexander&country_code=UA
Accept: application/json

###

GET http://localhost:80/api/users?verified=0
Accept: application/json

###
```

* Edit users
```
PATCH http://localhost:80/api/users/11/edit
Content-Type: application/json
Accept: application/json

{
  "name": "Alexander",
  "email": "alexander@gmail.com",
  "password": "password1",
  "country": "Ukraine"
}
```

* Delete users
```
DELETE http://localhost:80/api/users?id[0]=3&id[1]=4
Accept: application/json

###
```

### Projects

* Add projects (For correct testing add minimum 4 projects)
```
POST http://localhost:80/api/projects/create
Content-Type: application/json
Accept: application/json

{
  "name": "MyWebsite"
}

###

POST http://localhost:80/api/projects/create
Content-Type: application/json
Accept: application/json

{
  "name": "WorkProjectX"
}

###

POST http://localhost:80/api/projects/create
Content-Type: application/json
Accept: application/json

{
  "name": "Application"
}

###

POST http://localhost:80/api/projects/create
Content-Type: application/json
Accept: application/json

{
  "name": "Application1"
}

###
```

* Link projects to users
```
PUT http://localhost:80/api/projects/1/users?id[0]=10&id[1]=9&id[2]=8
Accept: application/json

###

PUT http://localhost:80/api/projects/2/users?id[0]=11
Accept: application/json

###

PUT http://localhost:80/api/projects/3/users?id[0]=11
Accept: application/json

###
```

* List projects incl. labels (filter by user.email OR/AND user.continent OR/AND labels)
```
GET http://localhost:80/api/projects?email=alexander@gmail.com
Accept: application/json

###

GET http://localhost:80/api/projects?continent=EU
Accept: application/json

###

GET http://localhost:80/api/projects?continent=SA
Accept: application/json

###

GET http://localhost:80/api/projects?label=Android
Accept: application/json

###
```

* Delete projects
```
DELETE http://localhost:80/api/projects?id[0]=1
Accept: application/json

###

DELETE http://localhost:80/api/projects?id[0]=2&id[1]=3
Accept: application/json

###
```

### Labels

* Add labels
```
POST http://localhost:80/api/labels/create
Content-Type: application/json
Accept: application/json

{
  "name": "personal"
}

###

POST http://localhost:80/api/labels/create
Content-Type: application/json
Accept: application/json

{
  "name": "wordpress"
}

###

POST http://localhost:80/api/labels/create
Content-Type: application/json
Accept: application/json

{
  "name": "Web"
}

###

POST http://localhost:80/api/labels/create
Content-Type: application/json
Accept: application/json

{
  "name": "Web1"
}

###
```

* Link labels to projects
```
PUT http://localhost:80/api/labels/1/projects?id[0]=1
Accept: application/json

###

PUT http://localhost:80/api/labels/2/projects?id[0]=2
Accept: application/json

###

PUT http://localhost:80/api/labels/3/projects?id[0]=3
Accept: application/json

###

PUT http://localhost:80/api/labels/4/projects?id[0]=4
Accept: application/json

###
```

* List labels (filter by user.email OR/AND projects)
```
GET http://localhost:80/api/labels?email=alexander@gmail.com
Accept: application/json

###

GET http://localhost:80/api/labels?id_project[0]=1&id_project[1]=2
Accept: application/json

###
```

* Delete labels
```
DELETE http://localhost:80/api/labels?id[0]=1
Accept: application/json

###

DELETE http://localhost:80/api/labels?id[0]=4&id[1]=5
Accept: application/json

###
```


## To exit leave the container and down services
* Pres ctrl+d in the container
* Write in the terminal: docker-compose down
