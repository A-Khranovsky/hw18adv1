<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::put('users/add',[\App\Http\Controllers\Api\UsersController::class, 'Add']);
Route::get('users/verify/{email}/{remember_token}', [\App\Http\Controllers\Api\UsersController::class, 'Verify']);
Route::get('users',[\App\Http\Controllers\Api\UsersController::class, 'List']);
Route::patch('users/{id}/edit',[\App\Http\Controllers\Api\UsersController::class, 'Edit']);
Route::delete('users',[\App\Http\Controllers\Api\UsersController::class, 'Destroy']);

Route::post('projects/create',[\App\Http\Controllers\Api\ProjectController::class, 'Create']);
Route::put('projects/{project_id}/users',[\App\Http\Controllers\Api\ProjectController::class, 'linkUser']);
Route::get('projects',[\App\Http\Controllers\Api\ProjectController::class, 'List']);
Route::delete('projects',[\App\Http\Controllers\Api\ProjectController::class, 'destroy']);

Route::post('labels/create',[\App\Http\Controllers\Api\LabelController::class, 'Create']);
Route::put('labels/{label_id}/projects',[\App\Http\Controllers\Api\LabelController::class, 'linkLabel']);
Route::get('labels',[\App\Http\Controllers\Api\LabelController::class, 'List']);
Route::delete('labels',[\App\Http\Controllers\Api\LabelController::class, 'Destroy']);
