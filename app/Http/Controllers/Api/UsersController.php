<?php


namespace App\Http\Controllers\Api;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Jobs\AddUser;

//use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersController
{
    public function Add(Request $request)
    {
        $country = Country::where('name', '=', $request->get('country'))->get()->first();

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'remember_token' => Str::random(10),
            'country_id' => $country->id
        ]);

        Auth::login($user);

        $data = [
            'email' => $request->get('email'),
            'token' => $user->createToken('Web')->plainTextToken
        ];

        AddUser::dispatch($data)->onQueue('Users');

        return response('User was successfully created', 200);
    }

    public function Verify($email, $remember_token)
    {
        $user = User::where('email', '=', $email)->first();

        if(!$user){
            return response(null, 404)
                ->json(['msg' => 'User not found.']);
        }
        if($remember_token !== $user->remember_token){
            return response(null ,422)
                ->json(['msg' => 'Wrong email or password']);
        }

        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();

        Auth::login($user);
        return $user->createToken('Web');
    }

    public function List(Request $request)
    {
        $query = User::query();
        if ($request->has('name')) {
            $query->where('name', '=', $request->get('name'));
        }
        if ($request->has('email')) {
            $query->where('email', '=', $request->get('email'));
        }
        if ($request->has('verified')) {
            $query->where('verified', '=', $request->get('verified'));
        }
        if ($request->has('country_code')) {
            $country_code = $request->get('country_code');
            $query->whereHas('country', function (Builder $qb) use ($country_code) {
                    $qb->where('code', $country_code);
            });

        }
        return response(new UserCollection($query->get()), 200);
    }

    public function Edit($id, Request $request)
    {
        $country = Country::where('name', '=', $request->get('country'))->get()->first();
        User::where('id', '=', $id)
            ->update(
                [
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                    'country_id' => $country->id
                ]);
        return response("User (id = $id) was successfully edited", 200);
    }

    public function destroy(Request $request)
    {
        $ids = $request->get('id');
        User::query()
            ->whereIn('id', $ids)->delete();
        return response('Users (id = '.implode(',',$ids).') was successfully deleted', 200);
    }
}
