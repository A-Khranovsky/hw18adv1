<?php


namespace App\Http\Controllers\Api;


use App\Http\Resources\LabelCollection;
use App\Models\Country;
use App\Models\Label;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class LabelController
{
    public function Create(Request $request)
    {
        Label::create([
            'name' => $request->get('name')
        ]);
        return response('Label was successfully created', 200);
    }

    public function List(Request $request)
    {
        $query = Label::query();

        if ($request->has('id_project')) {
            $id_projects = $request->get('id_project');
            $query->whereHas('projects', function (Builder $qb) use ($id_projects) {
                $qb->whereIn('projects.id', $id_projects);
            });
        }

        if ($request->has('email')) {
            $email = $request->get('email');
            $query->whereHas('projects', function (Builder $qb) use ($email) {
                $qb->whereHas('users', function (Builder $qb) use ($email) {
                    $qb->where('email', $email);
                });
            });
        }

        return new LabelCollection($query->get());
    }

    public function linkLabel($label_id, Request $request)
    {
        $label = Label::find($label_id);
        $ids = $request->get('id');

        $label->projects()->attach($ids);
        return response('Label id = ' . $label_id . ' was successfully linked to projects id = '
            . implode(',', $ids), 200);
    }

    public function destroy(Request $request)
    {
        $ids = $request->get('id');
        $labels = Label::whereIn('id', $ids)->get();
        foreach ($labels as $label) {
            $label->projects()->detach();
            $label->delete();
        }
        return response('Labels (id = ' . implode(',', $ids) . ') was successfully deleted', 200);
    }
}
